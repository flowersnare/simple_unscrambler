#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *unscramble(char *inputString, int size, signed int offset) {
    int i;
    for (i=0;i<size;i++) {
        //Checks if char is uppercase (ascii: 65-90)
        if ( (inputString[i] > 64) && (inputString[i] < 91)) {
            if (inputString[i] > (91-offset-1)) {
                inputString[i] -= 26;
            }
            inputString[i] += offset;
        }
        //Checks if char is lowercase
        else if ( (inputString[i] > 96) && (inputString[i] < 123)) {

            if (inputString[i] > 123-offset-1) {
                inputString[i] -= 26;
            }

            inputString[i] += offset;
        }
        
        else if ( (inputString[i] != 32) && (inputString[i] != 10) ) {
            printf("Error, input not a letter: %d - Pos: %d\n", inputString[i], i);
        }
    }
    //printf("%s\n", inputString);

    return (inputString);
}

//Not properly checing input
//Don't use for any production purpose
int getLine(char *inp) {
    char c;
    int of;
   if (fgets(inp, 130, stdin) == NULL) {
        return 1;
    }
    //Flushes the input if the last character is not line break
    if (inp[strlen(inp)-1] != "\n"){
        while ( ((c = getchar()) != "\n") && (c != EOF) );
    }
    //Repleace linebrake
    inp[strlen(inp)-1] == "/0";
    return 0;
}

int getInt() {
    char *inp;
    int retInt, len, i;

    if (fgets(inp, 130, stdin) == NULL) {
        return 1;
    }

    len = strlen(inp); 
    
    for(i=0;i<len;i++) {
        int isNum = 0;
        if (!( (inp[i] < 58) && (inp[i] > 47) )){
            isNum = 1; 
        }
    }

    if ((isNum == 0) && (inp != NULL)) {
        return (atoi(inp)); 
    }
    else {
        return 1;
    }

}

int main(int argc, char **argv) {
    if (argc == 4) {
        printf("%s\n", argv[1]);
        int size = atoi(argv[2]);
        int offset = atoi(argv[3]);
        printf("%s\n", unscramble(argv[1], size, offset));
    }
    else if ((argc > 1) && ((unsigned)(char)argv[1][0] == 45)) {
       if (strcmp(argv[1], "-h") == 0) {
            printf("Help menu!\n");
        } 
        else if (strcmp(argv[1], "-s") == 0) {
            printf("Enter string to descramble: ");
            char inputString[140];
            getLine(inputString);
            printf("%s\n", inputString);
            printf("%s\n", unscramble(inputString, strlen(inputString), atoi(argv[2])));
        }
    }
    else if (argc != 4) {
        printf("Error invalid input \n Format: cunscrambler <input Text> <input size>\n");
        return 1;
    }
    return 0;
}
