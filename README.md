# Simple Unscrambler



## Getting started

This is a simple program to solve a caesar cipher. Format as such: "cUnscrambler <input string> <string size> <offset>"

Compile in gcc: "gcc main.c -o cUnscrambler"
